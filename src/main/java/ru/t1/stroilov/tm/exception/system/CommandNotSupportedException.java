package ru.t1.stroilov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(@NotNull final String message) {
        super("Error! Command \"" + message + "\" not supported...");
    }
}
