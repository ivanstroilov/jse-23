package ru.t1.stroilov.tm.exception.user;

import ru.t1.stroilov.tm.exception.field.AbstractFieldException;

public final class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}
