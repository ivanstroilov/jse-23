package ru.t1.stroilov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public final static String DESCRIPTION = "Display allowed commands.";

    @NotNull
    public final static String NAME = "commands";

    @NotNull
    public final static String ARGUMENT = "-c";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            @NotNull final String name = command.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }
}
