package ru.t1.stroilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    public final static String DESCRIPTION = "Unbind Task from Project.";

    @NotNull
    public final static String NAME = "task-unbind-from-project";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("Enter Project ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter Task ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(getUserId(), projectId, taskId);
    }
}
